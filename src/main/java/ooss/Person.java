package ooss;

import java.util.Objects;

public class Person {
    protected int id;
    protected String name;
    protected int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce() {
        String introduce = "My name is " + this.name + ". " + "I am " + this.age + " years old.";
        return introduce;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id == person.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    // 观察者监听到变化后要产生的响应函数，将student和teacher里面重写。
    public void update(Klass klass) {
    }
}

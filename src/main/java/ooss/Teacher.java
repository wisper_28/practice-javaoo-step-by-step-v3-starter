package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {
    protected List<Klass> klasses;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klasses = new ArrayList<>();
    }

    public String introduce() {
        String introduce = "My name is " + this.name + ". " + "I am " + this.age + " years old. I am a teacher.";
        if (klasses.size() > 0) {
            introduce += " I teach Class ";
            introduce += klasses.get(0).number;
            if (klasses.size() > 1) {
                for (int i = 1; i < klasses.size(); i++) {
                    Klass klass = klasses.get(i);
                    introduce = introduce + ", " + klass.number;
                }
            }
            introduce += ".";
        }
        return introduce;
    }

    public void assignTo(Klass klass) {
        this.klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return this.klasses.contains(student.klass);
    }

    @Override
    public void update(Klass klass) {
        String introduce;
        introduce = "I am " + this.name + ", teacher of Class " + klass.number + ". " + "I know " + klass.leader.name + " become Leader.";
        System.out.println(introduce);
    }
}

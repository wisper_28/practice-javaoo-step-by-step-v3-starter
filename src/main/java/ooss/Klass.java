package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    protected int number;
    protected Student leader;
    protected List<Person> personList;

    public Klass(int number) {
        this.number = number;
        this.personList = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student student) {
        if (student.klass != null && student.klass.number == this.number) {
            this.leader = student;
            this.notifyObservers();// 发生变化时，调用通知函数。
        } else {
            System.out.printf("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        return this.leader == student;
    }

    // 先將person添加到personList里面，同时也是对观察者的注册。
    public void attach(Person person) {
        this.personList.add(person);
    }

    // 通知被观察者的函数。
    public void notifyObservers() {
        for (Person person : personList) {
            person.update(this);// 参数是当前的班级，因为做出的响应里面要用到班级的信息
        }
    }
}

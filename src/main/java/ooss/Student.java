package ooss;

public class Student extends Person {
    protected Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce() {
        String introduce;
        if (this.klass != null) {
            introduce = "My name is " + this.name + ". " + "I am " + this.age + " years old. I am a student.";
            if (this.klass.leader == this) {
                introduce += " I am the leader of class " + this.klass.number + ".";
            } else {
                introduce += " I am in class " + this.klass.number + ".";
            }
        } else {
            introduce = "My name is " + this.name + ". " + "I am " + this.age + " years old. I am a student.";
        }
        return introduce;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass == klass;
    }

    @Override
    public void update(Klass klass) {
        String introduce;
        introduce = "I am " + this.name + ", student of Class " + klass.number + ". " + "I know " + klass.leader.name + " become Leader.";
        System.out.println(introduce);
    }
}
